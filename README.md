créer votre namespace
k3s kubectl create namespace mon-namespace

modifier le fichier env.yaml avec vos informations, puis déployer le configmap dans votre namespace
k3s kubectl apply -f env.yaml -n mon-namespace
k3s kubectl -n mon-namespace get configmap

déployer une instance de postgre
k3s kubectl apply -f postgre.yml -n mon-namespace
k3s kubectl -n mon-namespace get pods

déployer un openldap LTB
k3s kubectl apply -f volumes.yml -n mon-namespace
k3s kubectl apply -f openldapLtb.yml -n mon-namespace

déployer l'ensemble des applications
k3s kubectl apply -f fd.yml -n mon-namespace
k3s kubectl apply -f sd.yml -n mon-namespace
k3s kubectl apply -f wp.yml -n mon-namespace

déployer lemon ldap ng 
k3s kubectl apply -f fd.yml -n mon-namespace

récupérer le nom du pod avec la commande 
k3s kubectl get pods -n mon-namespace
vous pouvez suivre les logs d'un container avec la commande
k3s kubectl logs llng-container -c llng-fastcgi -f

ajouter les services de type ingress pour exposer les différentes urls
k3s kubectl apply -f ingress.yml -n mon-namespace

ajouter la ligne ci dessous à votre /etc/hosts
127.0.0.1 auth.osxp.demo.k3s wp.osxp.demo.k3s fd.osxp.demo.k3s sd.osxp.demo.k3s manager.osxp.demo.k3s

et voilà vous avez une stack de gestion d'identité sur un cluster k8s auto hebergé
