mkdir llng-keys
openssl req -new -newkey rsa:4096 -keyout llng-keys/saml.key -nodes -out llng-keys/saml.pem -x509 -days 3650
openssl genrsa -out llng-keys/oidc.key 4096
openssl rsa -pubout -in llng-keys/oidc.key -out llng-keys/oidc_pub.key
k3s kubectl -n osxp create configmap llng-keys --from-file=llng-keys
rm -rf  llng-keys
